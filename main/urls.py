from django.conf.urls import url
from .views import index, generate_from_file, generate_from_csv

#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^generate-from-file/', generate_from_file, name='generate-from-file'),
    url(r'^generate-from-csv/', generate_from_csv, name='generate-from-csv'),
]
