from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from random import randint

response = {}
flag_not_complete = False
list_hari = ["senin", "selasa", "rabu", "kamis", "jumat"]
list_shift = ["pagi", "siang", "malam"]
list_pekerja = []
holidays = {"senin": [], "selasa": [], "rabu": [], "kamis": [], "jumat": []}
shift_pekerja = {}

def index(request):
	html = "main.html"
	return render(request, html, response)

#=====================================================================================

def generate_schedule(request, command, jumlah_shift):
	response['list_hari'] = list_hari

	banyak_shift = list_shift[:jumlah_shift]
	response['banyak_shift'] = banyak_shift

	for x in banyak_shift:
		shift_pekerja[x] = []

	for line in command:
		attr = line.split(",")
		nama = attr[0]
		list_pekerja.append(nama)

		holidays[attr[1].lower()].append(nama)
		
		shift_pilihan = attr[2].lower().split(";")
		for x in shift_pilihan:
			if x in shift_pekerja:
				shift_pekerja[x].append(nama)
			
	# inisiasi schedule sesuai preferens
	initSced = init_schedule(shift_pekerja)
	score_init = score(initSced)


	# Loop hingga nilai heuristik = 0
	temp_schedule = initSced.copy()

	temp_schedule = local_search(temp_schedule, score_init)

	if score(temp_schedule) < score_init:
		initSced = temp_schedule

	flag_not_complete = checkIncomplete(initSced)

	response['schedule'] = initSced
	response['flag_not_complete'] = flag_not_complete
	html = "result.html"
	return render(request, html, response)

#=====================================================================================

# Menghitung nilai heuristik
def score(schedule):
	# Constraint:
	# 1. Tidak ada pekerja dalam shift = 4
	# 2. Melanggar preferens holidays = 3
	# 3. Pekerja memiliki 2 atau 3 shift dalam sehari = 2
	# 4. Mendapat shift malam, diikuti shift pagi = 1

	score = 0

	for i in range(0,5):
		hari = list_hari[i]
		arr_pekerja=[]

		for shift in schedule[hari]:
			pekerja_dalam_shift = schedule[hari][shift]

			if len(pekerja_dalam_shift) == 0:
				score += 4
				continue

			for pekerja in schedule[hari][shift]:
				if pekerja in arr_pekerja:
					score += 2
				else:
					arr_pekerja.append(pekerja)
					
				if pekerja in holidays[hari]:
					score += 3
				elif "malam" in schedule[hari]:
					if shift == "malam" and hari != "jumat":
						besok = list_hari[i+1]
						if pekerja in schedule[besok]["pagi"]:
							score += 1
					elif shift == "pagi" and hari != "senin":
						kemarin = list_hari[i-1]
						if pekerja in schedule[kemarin]["malam"]:
							score += 1
	return score

#=====================================================================================

def init_schedule(daftar_shift):
	schedule = {"senin": {}, "selasa": {}, "rabu": {}, "kamis": {}, "jumat": {}}

	for day in schedule:
		for jam in daftar_shift:
			schedule[day][jam] = []
			copy_daftar = daftar_shift[jam][:]

			iterasi = 1
			while iterasi < 4:
				nama = ""

				if len(copy_daftar) == 0:
					break
				elif len(copy_daftar) == 1:
					nama = copy_daftar[0]
				else:
					index = randint(0, len(copy_daftar) - 1)
					nama = daftar_shift[jam][index]

				if nama in schedule[day][jam]:
					iterasi -= 1
				else:
					schedule[day][jam].append(nama)
					copy_daftar.remove(nama)

				iterasi += 1

	return schedule

#=====================================================================================

def local_search(schedule, init_score):
	if init_score == 0:
		return schedule

	final_schedule = schedule.copy()
	final_score = 0
	for i in range(0,5):
		final_score = score(final_schedule)

		temp_schedule = final_schedule.copy()
		modify_schedule(temp_schedule, i)

		new_score = score(temp_schedule)
		if new_score >= final_score:
			return final_schedule
		else:
			final_schedule = local_search(temp_schedule, new_score)
	
	if final_score >= init_score:
		return schedule

	return final_schedule

#=====================================================================================

def modify_schedule(schedule, index):
	hari = list_hari[index]
	arr_pekerja=[]

	for shift in schedule[hari]:
		pekerja_dalam_shift = schedule[hari][shift]

		if len(pekerja_dalam_shift) < 3:
			flag_not_complete = True
			continue

		for pekerja in pekerja_dalam_shift:
			copy_daftar = shift_pekerja[shift][:]
			
			if pekerja in holidays[hari] or pekerja in arr_pekerja:
				pekerja_dalam_shift.remove(pekerja)
				copy_daftar.remove(pekerja)

				while len(copy_daftar) != 0:
					index = randint(0,len(copy_daftar) - 1)
					new = copy_daftar[index]
					if new != pekerja and new not in pekerja_dalam_shift:
						pekerja_dalam_shift.append(new)
						return
					else:
						copy_daftar.remove(new)

			elif "malam" in schedule[hari]:
				if shift == "malam" and hari != "jumat":
					besok = list_hari[index+1]
					if pekerja in schedule[besok]["pagi"]:
						pekerja_dalam_shift.remove(pekerja)
						copy_daftar.remove(pekerja)

						while len(copy_daftar) != 0:
							index = randint(0,len(copy_daftar) - 1)
							new = copy_daftar[index]
							if new != pekerja and new not in pekerja_dalam_shift:
								pekerja_dalam_shift.append(new)
								return
							else:
								copy_daftar.remove(new)
				elif shift == "pagi" and hari != "senin":
					kemarin = list_hari[index-1]
					if pekerja in schedule[kemarin]["malam"]:
						pekerja_dalam_shift.remove(pekerja)
						copy_daftar.remove(pekerja)

						while len(copy_daftar) != 0:
							index = randint(0,len(copy_daftar) - 1)
							new = copy_daftar[index]
							if new != pekerja and new not in pekerja_dalam_shift:
								pekerja_dalam_shift.append(new)
								return
							else:
								copy_daftar.remove(new)
			else:
				arr_pekerja.append(pekerja)


#=====================================================================================

def checkIncomplete(schedule):
	for hari in list_hari:
		for shift in schedule[hari]:
			if len(schedule[hari][shift]) < 3:
				return True

#=====================================================================================

@csrf_exempt
def generate_from_file(request):
	command = []
	if request.method == 'POST':
		jumlah_shift = request.POST['jumlah-shift']
		inputfile = request.FILES['input-file']

		for line in open(inputfile):
			line = line.strip('\r\n')
			command.append(line)

	return generate_schedule(request, command, jumlah_shift)

#=====================================================================================

@csrf_exempt
def generate_from_csv(request):
	command = []
	if request.method == 'POST':
		jumlah_shift = int(request.POST['jumlah-shift'])
		inputcsv = request.POST['input-csv']

		for line in inputcsv.split("\r\n"):
			command.append(line)

	return generate_schedule(request, command, jumlah_shift)