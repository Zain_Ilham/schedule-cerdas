# Tugas Akhir - Kelas SC A - Kelompok Sabeb

 Nurse Scheduling Problem Programming
 @Faculty of Computer Science Universitas Indonesia, Odd Semester 2018/2019

Anggota Kelompok:
	- Adib Yusril Wafi
	- Mohammad Ahnaf Zain Ilham
	- Tio Bagas Sulistiyanto
	

Link herokuapp:
https://schedule-cerdas.herokuapp.com/

Link Repo:
https://gitlab.com/Zain_Ilham/schedule-cerdas.git